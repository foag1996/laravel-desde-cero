<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProductosController extends Controller
{
    public function index (){
        return view ('productos.index');
    }

    public function create (){
        return 'Formulario para crear la Lista de Productos Controlador';
    }

    public function store (){
        //
    }

    public function show ($productos){
        return view ('productos.show');
    }

    public function edit ($productos){
        return "Mostrando el formulario para editar Productos con el ID Controlador {$productos}";
    }

    public function update ($productos){
        //
    }

    public function destroy ($productos){
        //
    }








}
