<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductosTabla extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('productos_tabla', function (Blueprint $table) {
            $table->id();
            $table->string('Titulo');
            $table->string('Descripcion', 1000);
            $table->float('Precio')->unsigned();
            $table->integer('Cantidad')->unsigned();
            $table->string('Estado')->default('unavailable');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('productos_tabla');
    }
}
